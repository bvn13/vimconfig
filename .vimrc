
" Setting on Vundle (vim packet manager)
set nocompatible
filetype off " обязательно!
"set rtp+=~/.vim/bundle/vundle/
if has('win32') || has('win64')
    set rtp+=$VIM/bundle/vundle/
    call vundle#rc('$VIM/bundle/')
else
    set rtp+=~/.vim/bundle/vundle/
    call vundle#rc()
endif
call vundle#rc()
filetype plugin indent on " обязательно!

" VIM Vundle autoload
Bundle 'tpope/vim-fugitive'
Bundle 'rstacruz/sparkup'
Bundle 'yko/mojo.vim'
Bundle 'scrooloose/nerdtree'
Bundle 'vtreeexplorer'
" Bundle 'project'
Bundle 'ide'
Bundle 'bufexplorer.zip'
Bundle 'minibufexpl.vim'
Bundle 'mru.vim'
Bundle 'taglist.vim'
Bundle 'CCTree'
Bundle 'nanotech/jellybeans.vim'
Bundle 'c9s/perlomni.vim'
Bundle 'Lokaltog/powerline'

runtime! bundle/*/*.vim
runtime! unbundled/*/*.vim
runtime! setup/*.vim

colorscheme jellybeans

