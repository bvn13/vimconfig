syntax on
filetype plugin indent on
set scrolloff=5
set background=dark

set hidden
set mouse=a
set mousehide
set showcmd
set matchpairs+=<:>
set showmatch
set autoread
set t_Co=256
set confirm
set ruler

autocmd! bufwritepost $MYVIMRC source $MYVIMRC

autocmd! bufreadpost * call LastPosition()
function! LastPosition()
    if line("'\"") && line("'\"")<=line('$')
        normal! `"
    endif
endfunction

set backspace=indent,eol,start
set sessionoptions=curdir,buffers,tabpages

set browsedir=current
set noerrorbells
set novisualbell
set t_vb=
set tm=500
set pastetoggle=

set ignorecase
set smartcase

set hlsearch
set incsearch

set lazyredraw
set magic

set foldenable
set foldmethod=syntax
set foldcolumn=3
set foldlevel=1
let perl_folding=1
let php_folding=1
set foldopen=all

set shiftwidth=4
set tabstop=4
set softtabstop=4
set autoindent
set cindent
set expandtab
set smartindent
set wrap

au FileType crontab,fstab,make set noexpandtab, tabstop=8 shiftwidth=8

if has('win32')
    set encoding=cp1251
else 
    set encoding=utf-8
    set termencoding=utf-8
endif
set ffs=unix,dos,mac
set fencs=utf-8,cp1251,koi8-r,cp866

filetype on
filetype plugin on
filetype indent on

autocmd! BufEnter *.pl compiler perl

if has('win32')
    let $VIMRUNTIME = $HOME.'\Programs\Vim\vim72'
    source $VIMRUNTIME/mswin.vim
else
    let $VIMRUNTIME = $HOME.'/.vim'
endif

if has('gui')
    if has('win32')
        set guifont=Lucida_Console:h10:cRUSSIAN::
    else
        set guifont=Terminus\ 10
    endif
    
    "set guioptions-=T
    au GUIEnter * :set lines=24 columns=50
endif

set backup
autocmd! bufwritepre * call BackupDir()
function! BackupDir()
    if has('win32')
        let l:backupdir = $VIMRUNTIME.'\backup'
    else
        let l:backupdir = $VIMRUNTIME.'/backup/'.
	        \substitute(expand('%:p:h'), '^'.$HOME, '~', '')
    endif
    if !isdirectory(l:backupdir)
        call mkdir(l:backupdir, 'p', 0700)
    endif
    let backupdir=l:backupdir
    let backupext=strftime('~%Y-%m-%d~')
endfunction

set nu!

set omnifunc=syntaxcomplete#Complete
autocmd FileType css set omnifunc=csscomplete#CompleteCSS

set laststatus=2
"set statusline=\ %{HasPaste()}%F%m%r%h\ %w\ \ CWD:\ %r%{getcwd()}%h\ \ \ Line:\ %l
set statusline=%F%m%r%h%w\ \ [TYPE=%Y]\ [ASCII=\%03.3b]\ [HEX=\%02.2B]\ [POS=%04l,%04v][%p%%]\ [LEN=%L]


" Delete trailing white space on save, useful for python and CoffeeScript
func! DeleteTrailingWS()
    exe "normal mz"
    %s/\s\+$//ge
    exe "normal `z"
endfunc
autocmd BufWrite *.py :call DeleteTrailingWS()
autocmd BufWrite *.coffee :call DeleteTrailingWS()


"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" => Helper functions
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
function! CmdLine(str)
    exe "menu Foo.Bar :" . a:str
    emenu Foo.Bar
    unmenu Foo
endfunction 

function! VisualSelection(direction, extra_filter) range
    let l:saved_reg = @"
    execute "normal! vgvy"

    let l:pattern = escape(@", '\\/.*$^~[]')
    let l:pattern = substitute(l:pattern, "\n$", "", "")

    if a:direction == 'b'
        execute "normal ?" . l:pattern . "^M"
    elseif a:direction == 'gv'
        call CmdLine("vimgrep " . '/'. l:pattern . '/' . ' **/*.' . a:extra_filter)
    elseif a:direction == 'replace'
        call CmdLine("%s" . '/'. l:pattern . '/')
    elseif a:direction == 'f'
        execute "normal /" . l:pattern . "^M"
    endif

    let @/ = l:pattern
    let @" = l:saved_reg
endfunction

" Returns true if paste mode is enabled
function! HasPaste()
    if &paste
        return 'PASTE MODE  '
    en
    return ''
endfunction
