" HOTKEYS

" Toggle lines numbers
inoremap <C-F12> <esc>:set<space>nu!<cr>a
noremap <C-F12> :set<space>nu!<cr>

" Quick save
nmap <F2> :w<cr>
vmap <F2> <esc>:w<cr>a
imap <F2> <esc>:w<cr>a

" Buffers list. Remapped by buffexplorer
"nmap <F5> <esc>:buffers<cr>
"vmap <F5> <esc>:buffers<cr>
"imap <F5> <esc><esc>:buffers<cr>

" Previous buffer
map <F6> :bp<cr>
vmap <F6> <esc>:bp<cr>i
imap <F6> <esc>:bp<cr>i

" Next buffer
map <F7> :bn<cr>
vmap <F7> <esc>:bn<cr>i
imap <F7> <esc>:bn<cr>i

" Toggle lines numbers
imap <F11> <esc>:set<Space>nu!<cr>a
nmap <F11> <esc>:set<Space>nu!<cr>a

" Working with tabs
map <S-tab> :tabprevious<cr>
nmap <S-tab> :tabprevious<cr>
imap <S-tab> <esc>:tabprevious<cr>i

map <C-tab> :tabnext<cr>
nmap <C-tab> :tabnext<cr>
imap <C-tab> <esc>:tabnext<cr>i

nmap <C-t> :tabnew<cr>
imap <C-t> <esc>:tabnew<cr>i

nmap <C-w> :tabclose<cr>
imap <C-w> <esc>:tabclose<cr>

" Duplicate current line
imap <C-d> <esc>yypa

vnoremap <C-X> "+x
vnoremap <C-C> "+y
map <C-V>      "+gP

" Ctrl-Z = undo, Ctrl-Y = redo
noremap <C-Z> u
inoremap <C-Z> <C-O>u
noremap <C-Y> <C-R>
inoremap <C-Y> <C-O><C-R>

" Quick encoding menu
set wildmenu
set wcm=<tab>
menu Encoding.CP1251 	:e ++enc=cp1251<cr>
menu Encoding.CP866 	:e ++enc=cp866<cr>
menu Encoding.KOI8-U 	:e ++enc=koi8-u<cr>
menu Encoding.UTF-8 	:e ++enc=utf-8<cr>
map <C-E> :emenu Encoding.<tab>

" Quick quit menu
set wildmenu
set wcm=<tab>
menu Quit.Save_All_and_Stand        :wa<cr>
menu Quit.Save_and_Quit             :w<cr>:confirm qa<cr>
menu Quit.Save_All_and_Quit         :wa<cr>:qa<cr>
menu Quit.Dont_Save_All_and_Quit    :qa<cr>
map <C-F10> :emenu Quit.<tab>

" Visual mode pressing * or # searches for the current selection
vnoremap <silent> * :call VisualSelection('f', '')<cr>
vnoremap <silent> # :call VisualSelection('b', '')<cr>

" Treat long lines as break lines (useful when moving around in them)
map j gj
map k gk

" Move a line of text using ALT+[jk] or Command+[jk] on mac
nmap <M-j> mz:m+<cr>`z
nmap <M-k> mz:m-2<cr>`z
vmap <M-j> :m'>+<cr>`<my`>mzgv`yo`z
vmap <M-k> :m'<-2<cr>`>my`<mzgv`yo`z

if has("mac") || has("macunix")
    nmap <D-j> <M-j>
    nmap <D-k> <M-k>
    vmap <D-j> <M-j>
    vmap <D-k> <D-k>
endif
